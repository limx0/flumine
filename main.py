import json
import logging
import os
import sys

import betfairlightweight
import fire as fire
from betfairlightweight import APIClient

from flumine import Flumine, FlumineException, __version__
from flumine.resources import MarketRecorder, RaceRecorder
from flumine.storage import storageengine
from flumine import utils


def setup_logging():
    logger = logging.getLogger("betfairlightweight")
    logger.setLevel(logging.INFO)
    logging.basicConfig(
        format="%(asctime)s | %(levelname)s | %(message)s | %(filename)s | %(module)s",
        level=logging.INFO,
    )


def run_flumine(market_filter, stream_type='market'):
    setup_logging()

    logging.info("betfairlightweight version: %s" % betfairlightweight.__version__)
    logging.info("flumine version: %s" % __version__)

    if stream_type == "race":
        logging.info('Creating "storageengine.s3"')
        storage_engine = storageengine.S3(
            "flumine", data_type="racedata", force_update=False
        )
        logging.info('Creating "RaceRecorder"')
        recorder = RaceRecorder(storage_engine=storage_engine)
    elif stream_type == "market":
        try:
            market_data_filter = json.loads(sys.argv[3])
        except IndexError:
            logging.warning("Market Data Filter not provided, defaulting to None")
            market_data_filter = None
        except json.JSONDecodeError:
            logging.error("Market Data Filter arg must be provided in json format")
            market_data_filter = None

        logging.info('Creating "storageengine.s3"')
        # storage_engine = storageengine.S3("flumine", data_type="marketdata")
        storage_engine = storageengine.Local(directory=os.environ['BETFAIR_STORAGE_DIR'])

        logging.info('Creating "MarketRecorder"')
        recorder = MarketRecorder(
            storage_engine=storage_engine,
            market_filter=market_filter,
            market_data_filter=market_data_filter,
        )
    else:
        raise ValueError('Invalid stream_type must be "race" or "market"')

    flumine = Flumine(
        recorder=recorder,
        settings={
            'betfairlightweight': dict(
                username=os.environ['BETFAIR_USERNAME'],
                password=os.environ['BETFAIR_PASSWORD'],
                app_key=os.environ['BETFAIR_APP_KEY'],
                certs=os.environ['BETFAIR_CERT_DIR'],
            ),
        }
    )
    try:
        flumine.start(async_=False)
    except FlumineException as e:
        logging.critical("Major flumine error: %s" % e)


def main(market_func_name='get_nba_markets'):
    client = APIClient(
        username=os.environ['BETFAIR_USERNAME'],
        password=os.environ['BETFAIR_PASSWORD'],
        app_key=os.environ['BETFAIR_APP_KEY'],
        certs=os.environ['BETFAIR_CERT_DIR'],
    )
    client.login()

    market_func = getattr(utils, market_func_name)

    markets = market_func(client=client)
    logging.info(f'Found {len(markets)} markets for {market_func_name}')
    run_flumine(market_filter={'eventIds': [mkt['event']['id'] for mkt in markets]})


if __name__ == "__main__":
    fire.Fire(main)
