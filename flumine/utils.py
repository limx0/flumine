import uuid


def create_short_uuid():
    return str(uuid.uuid4())[:8]


def get_nba_markets(client):
    market_catalogue = client.betting.list_market_catalogue(
        filter={"eventTypeIds": ["7522"]},
        market_projection=['COMPETITION', 'EVENT', 'MARKET_START_TIME'],
        max_results=1000,
        lightweight=True,
    )
    return [x for x in market_catalogue if x['competition']['name'] == 'NBA']


def get_tennis_markets(client):
    market_catalogue = client.betting.list_market_catalogue(
        filter={"eventTypeIds": ["2"]},
        market_projection=['COMPETITION', 'EVENT', 'MARKET_START_TIME'],
        max_results=1000,
        lightweight=True,
    )
    return market_catalogue
