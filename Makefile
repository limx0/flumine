IMAGE?=limx0/zeljko/flumine

docker-build:
	docker build -t ${IMAGE} .

docker-build-force:
	docker build -t ${IMAGE} --no-cache .

docker-push:
	docker push ${IMAGE}

undeploy:
	kubectl delete -f ./deploy/nba_deployment.yaml || true
	kubectl delete -f ./deploy/tennis_deployment.yaml || true


deploy: undeploy
	kubectl apply -f ./deploy/tennis_deployment.yaml
	kubectl apply -f ./deploy/nba_deployment.yaml
